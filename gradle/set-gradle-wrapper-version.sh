#!/usr/bin/env bash
gradle wrapper --gradle-version="$1"
#
# or (if you do not have installed gradle):
#./gradlew wrapper --gradle-version="$1"
#