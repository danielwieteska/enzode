package com.enzode.stockphoto.photoservices.jsonplaceholder.mapping;

import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphAddress;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphCompany;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphGeo;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphUser;

class ExampleObjects {

    static JphGeo exampleJphGeo() {
        return new JphGeo("45.89", "-56.8956");
    }

    static JphAddress exampleJphAddress() {
        return JphAddress.builder()
                .city("City1")
                .street("Street1")
                .suite("Suite1")
                .zipCode("ZipCode1")
                .geo(exampleJphGeo())
                .build();
    }

    static JphCompany exampleJphCompany() {
        return JphCompany.builder()
                .name("Name1")
                .bs("bs1")
                .catchPhrase("catchPhrase1")
                .build();
    }

    static JphUser exampleJphUser() {
        return JphUser.builder()
                .email("email@wp.pl")
                .phone("123456789")
                .name("Name1")
                .username("Username1")
                .website("https://google.com")
                .company(ExampleObjects.exampleJphCompany())
                .address(ExampleObjects.exampleJphAddress())
                .build();
    }
}
