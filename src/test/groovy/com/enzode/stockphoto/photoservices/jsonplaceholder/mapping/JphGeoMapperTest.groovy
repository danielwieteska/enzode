package com.enzode.stockphoto.photoservices.jsonplaceholder.mapping

import com.enzode.stockphoto.domain.model.Geo
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphGeo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spock.lang.Unroll

@SpringBootTest
class JphGeoMapperTest extends Specification {
    @Autowired
    JphGeoMapper jphGeoMapper

    @Unroll
    def "test Geo mapper"() {
        when:
        JphGeo jphGeo = new JphGeo(lat, lng)
        Geo geo = jphGeoMapper.jphGeoToGeo(jphGeo)

        then:
        geo.getLat().scale() == 4
        geo.getLng().scale() == 4
        geo.getLat().precision() == latPrecision
        geo.getLng().precision() == lngPrecision

        where:
        lat        | lng       || latPrecision | lngPrecision
        "-37.2589" | "48.8956" || 6            | 6
        "-1.0"     | "5.58"    || 5            | 5
        "8"        | "49"      || 5            | 6
        "0.8"      | "-0.96"   || 4            | 4
    }
}
