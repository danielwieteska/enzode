package com.enzode.stockphoto.photoservices.jsonplaceholder.mapping

import com.enzode.stockphoto.domain.model.Address
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphAddress
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import java.math.RoundingMode

@SpringBootTest
class JphAddressMapperTest extends Specification {
    @Autowired
    JphAddressMapper jphUserMapper

    def "test Address mapper"() {
        when:
        JphAddress jphAddress = ExampleObjects.exampleJphAddress()
        Address address = jphUserMapper.jphAddressToAddress(jphAddress)

        then:
        address.getId() == null
        address.getCity() == "City1"
        address.getStreet() == "Street1"
        address.getSuite() == "Suite1"
        address.getZipCode() == "ZipCode1"
        address.getGeo().getLat() == new BigDecimal("45.89").setScale(4, RoundingMode.HALF_UP)
        address.getGeo().getLng() == new BigDecimal("-56.8956").setScale(4, RoundingMode.HALF_UP)
    }
}
