package com.enzode.stockphoto.photoservices.jsonplaceholder.mapping

import com.enzode.stockphoto.domain.model.User
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphUser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import java.math.RoundingMode

@SpringBootTest
class JphUserMapperTest extends Specification {
    @Autowired
    JphUserMapper jphUserMapper

    def "test User mapper"() {
        when:
        JphUser jphUser = ExampleObjects.exampleJphUser()
        User user = jphUserMapper.jphUserToUser(jphUser)

        then:
        user.getId() == null
        user.getName() == "Name1"
        user.getUsername() == "Username1"
        user.getPhone() == "123456789"
        user.getEmail() == "email@wp.pl"
        user.getWebsite().toString() == "https://google.com"

        user.getAddress().getId() == null
        user.getAddress().getCity() == "City1"
        user.getAddress().getStreet() == "Street1"
        user.getAddress().getSuite() == "Suite1"
        user.getAddress().getZipCode() == "ZipCode1"
        user.getAddress().getGeo().getLat() == new BigDecimal("45.89").setScale(4, RoundingMode.HALF_UP)
        user.getAddress().getGeo().getLng() == new BigDecimal("-56.8956").setScale(4, RoundingMode.HALF_UP)

        user.getCompany().getId() == null
        user.getCompany().getName() == "Name1"
        user.getCompany().getCatchPhrase() == "catchPhrase1"
        user.getCompany().getBs() == "bs1"
    }
}
