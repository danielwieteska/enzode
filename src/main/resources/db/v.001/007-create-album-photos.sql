create table stock_photo.album_photos
(
    album_id bigint references stock_photo.album (id),
    photo_id bigint references stock_photo.photo (id)
);