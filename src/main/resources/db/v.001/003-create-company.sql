create table stock_photo.company
(
    id           bigserial primary key,
    name         varchar(50),
    catch_phrase varchar(50),
    bs           varchar(50)
);
create unique index on stock_photo.company (name);