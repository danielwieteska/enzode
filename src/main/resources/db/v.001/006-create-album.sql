create table stock_photo.album
(
    id      bigserial primary key,
    user_id bigint references stock_photo.user (id),
    title   varchar(150)
);
create index on stock_photo.album (title);
create unique index on stock_photo.album (user_id, title);