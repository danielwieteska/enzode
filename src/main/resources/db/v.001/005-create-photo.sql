create table stock_photo.photo
(
    id            bigserial primary key,
    user_id       bigint references stock_photo.user (id),
    title         varchar(100),
    url           varchar(150),
    thumbnail_url varchar(150)
);
create index on stock_photo.photo (title);
create index on stock_photo.photo (url);
create index on stock_photo.photo (thumbnail_url);