create table stock_photo.address
(
    id       bigserial primary key,
    street   varchar(50),
    suite    varchar(50),
    city     varchar(50),
    zip_code varchar(50),
    geo_lat  varchar(50),
    geo_lng  varchar(50)
);
create unique index on stock_photo.address (street, suite, city, zip_code);