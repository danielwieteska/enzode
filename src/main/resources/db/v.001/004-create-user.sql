create table stock_photo.user
(
    id         bigserial primary key,
    address_id bigint references stock_photo.address (id),
    company_id bigint references stock_photo.company (id),
    name       varchar(50),
    username   varchar(50),
    password   varchar(150),
    role       varchar(50),
    email      varchar(50),
    phone      varchar(50),
    website    varchar(50)
);