package com.enzode.stockphoto.domain.repositories;

import com.enzode.stockphoto.domain.model.User;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    User findByNameAndUsernameAndEmailAndPhoneAndWebsite(
            String name, String surname, String email, String phone, String website
    );

    default User findUser(JphUser jphUser) {
        return findByNameAndUsernameAndEmailAndPhoneAndWebsite(
                jphUser.getName(),
                jphUser.getUsername(),
                jphUser.getEmail(),
                jphUser.getPhone(),
                jphUser.getWebsite()
        );
    }
}
