package com.enzode.stockphoto.domain.repositories;

import com.enzode.stockphoto.domain.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AddressRepository extends JpaRepository<Address, Long> {
    Optional<Address> findByStreetAndSuiteAndCityAndZipCode(
            String street, String suite, String city, String zipCode
    );

    default Optional<Address> findAddress(Address address) {
        return findByStreetAndSuiteAndCityAndZipCode(
                address.getStreet(),
                address.getSuite(),
                address.getCity(),
                address.getZipCode()
        );
    }
}
