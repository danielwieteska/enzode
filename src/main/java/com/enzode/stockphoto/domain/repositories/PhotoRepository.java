package com.enzode.stockphoto.domain.repositories;

import com.enzode.stockphoto.domain.model.Photo;
import com.enzode.stockphoto.apiphoto.PhotoExposed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface PhotoRepository extends JpaRepository<Photo, Long> {

    Optional<Photo> findByUrl(String url);

    @Query(
            nativeQuery = true,
            value = "select p.id as photoId, p.title as photoTitle, p.url as photoUrl, " +
                    "p.thumbnail_url as photoThumbnailUrl, a.title as albumTitle " +
                    "from stock_photo.album_photos as ap " +
                    "left join stock_photo.photo as p on ap.photo_id = p.id " +
                    "left join stock_photo.album as a on ap.album_id = a.id " +
                    "where p.title like CONCAT('%', :photoTitle, '%') " +
                    "and p.url like CONCAT('%', :photoUrl, '%') " +
                    "and p.thumbnail_url like CONCAT('%', :photoThumbnailUrl, '%') " +
                    "and a.title like CONCAT('%', :albumTitle, '%')"
    )
    List<PhotoExposed> findPhotoUnsafe(
            @Param("photoTitle") @NotNull String photoTitle,
            @Param("photoUrl") @NotNull String photoUrl,
            @Param("photoThumbnailUrl") @NotNull String photoThumbnailUrl,
            @Param("albumTitle") @NotNull String albumTitle
    );

    default List<PhotoExposed> findPhotoSafe(
            @Nullable String photoTitle,
            @Nullable String photoUrl,
            @Nullable String photoThumbnailUrl,
            @Nullable String albumTitle
    ) {
        return findPhotoUnsafe(
                photoTitle == null ? "%" : photoTitle,
                photoUrl == null ? "%" : photoUrl,
                photoThumbnailUrl == null ? "%" : photoThumbnailUrl,
                albumTitle == null ? "%" : albumTitle
        );
    }
}
