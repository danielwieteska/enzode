package com.enzode.stockphoto.domain.repositories;

import com.enzode.stockphoto.domain.model.Album;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface AlbumRepository extends JpaRepository<Album, Long> {
    Set<Album> findByUserId(Long userId);
    Album findByUserIdAndTitle(Long userId, String title);
}
