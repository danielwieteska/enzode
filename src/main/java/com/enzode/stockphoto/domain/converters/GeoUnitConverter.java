package com.enzode.stockphoto.domain.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

@Component
public class GeoUnitConverter implements AttributeConverter<BigDecimal, String> {
    private final DecimalFormat decimalFormatFourZerosPrecision;
    private final EncryptionConverter encryptionConverter;

    @Autowired
    public GeoUnitConverter(EncryptionConverter encryptionConverter) {
        this.encryptionConverter = encryptionConverter;
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatFourZerosPrecision = new DecimalFormat("#,##0.0000", decimalFormatSymbols);
    }

    @Override
    public String convertToDatabaseColumn(BigDecimal bigDecimal) {
        if (bigDecimal == null) return null;
        String bigDecimalAsString = decimalFormatFourZerosPrecision.format(bigDecimal);
        return encryptionConverter.convertToDatabaseColumn(bigDecimalAsString);
    }

    @Override
    public BigDecimal convertToEntityAttribute(String encryptedBigDecimalAsString) {
        if (encryptedBigDecimalAsString == null) return null;
        String bigDecimalAsString = encryptionConverter.convertToEntityAttribute(encryptedBigDecimalAsString);
        return new BigDecimal(bigDecimalAsString).setScale(4, RoundingMode.HALF_UP);
    }
}
