package com.enzode.stockphoto.domain.converters;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.AttributeConverter;
import java.security.InvalidKeyException;
import java.security.Key;
import java.util.Base64;

@Getter
@Component
public class EncryptionConverter implements AttributeConverter<String, String> {
    private final Key key;
    private final Cipher cipher;

    public EncryptionConverter(
            @Value("${stock-photo.aes-secret}") String secret
    ) throws Exception {
        this.key = new SecretKeySpec(secret.getBytes(), "AES");
        this.cipher = Cipher.getInstance("AES");
    }

    @Override
    public String convertToDatabaseColumn(String plainTextData) {
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encryptedDataAsBytes = cipher.doFinal(plainTextData.getBytes());
            return Base64.getEncoder().encodeToString(encryptedDataAsBytes);
        } catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public String convertToEntityAttribute(String encryptedData) {
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedDataAsBytes = Base64.getDecoder().decode(encryptedData);
            byte[] decryptedDataAsBytes = cipher.doFinal(encryptedDataAsBytes);
            return new String(decryptedDataAsBytes);
        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            throw new IllegalStateException(e);
        }
    }
}
