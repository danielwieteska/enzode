package com.enzode.stockphoto.domain.model;

import com.enzode.stockphoto.domain.converters.EncryptionConverter;
import com.enzode.stockphoto.domain.converters.PasswordConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user", schema = "stock_photo")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Exclude
    private Long id;
    @ManyToOne(optional = false, cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "address_id")
    private Address address;
    @ManyToOne(optional = false, cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "company_id")
    private Company company;
    @Column
    @Convert(converter = EncryptionConverter.class)
    private String name;
    @Column
    private String username;
    @Column
    @EqualsAndHashCode.Exclude
    @Convert(converter = PasswordConverter.class)
    private String password;
    @Column
    @EqualsAndHashCode.Exclude
    private String role;
    @Column
    @Convert(converter = EncryptionConverter.class)
    private String email;
    @Column
    @Convert(converter = EncryptionConverter.class)
    private String phone;
    @Column
    @Convert(converter = EncryptionConverter.class)
    private String website;
}
