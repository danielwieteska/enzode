package com.enzode.stockphoto.domain.model;

import com.enzode.stockphoto.domain.converters.GeoUnitConverter;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Data
@Embeddable
public class Geo {
    @Column(name = "geo_lat")
    @Convert(converter = GeoUnitConverter.class)
    private BigDecimal lat;
    @Column(name = "geo_lng")
    @Convert(converter = GeoUnitConverter.class)
    private BigDecimal lng;
}
