package com.enzode.stockphoto.domain.model;

import com.enzode.stockphoto.domain.converters.EncryptionConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "address", schema = "stock_photo")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Exclude
    private Long id;
    @Column
    @Convert(converter = EncryptionConverter.class)
    private String street;
    @Column
    @Convert(converter = EncryptionConverter.class)
    private String suite;
    @Column
    @Convert(converter = EncryptionConverter.class)
    private String city;
    @Column(name = "zip_code")
    @Convert(converter = EncryptionConverter.class)
    private String zipCode;
    @Embedded
    private Geo geo;
}
