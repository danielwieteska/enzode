package com.enzode.stockphoto.apiphoto;

import com.enzode.stockphoto.domain.repositories.PhotoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1.0/photos")
public class PhotoController {
    private final PhotoRepository photoRepository;

    @GetMapping
    public List<PhotoExposed> getPhotos(
            @RequestParam(required = false) String photoTitle,
            @RequestParam(required = false) String photoUrl,
            @RequestParam(required = false) String photoThumbnailUrl,
            @RequestParam(required = false) String albumTitle
    ) {
        return photoRepository.findPhotoSafe(
                photoTitle, photoUrl, photoThumbnailUrl, albumTitle
        );
    }
}
