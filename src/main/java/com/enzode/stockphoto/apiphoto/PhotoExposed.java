package com.enzode.stockphoto.apiphoto;

public interface PhotoExposed {
    Long getPhotoId();
    String getAlbumTitle();
    String getPhotoTitle();
    String getPhotoUrl();
    String getPhotoThumbnailUrl();
}
