package com.enzode.stockphoto.configswagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {
    @Bean
    public Docket swaggerApi10() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("stock-photo-1.0")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.enzode.stockphoto.apiphoto"))
                .paths(PathSelectors.ant("/api/v1.0/**"))
                .build()
                .apiInfo(apiInfo10());
    }

    private ApiInfo apiInfo10() {
        return new ApiInfoBuilder()
                .version("1.0")
                .title("Stock Photos API")
                .description("Service which aggregate all stock photos in the world")
                .contact(new Contact("Daniel Wieteska", "https://example.com", "wieteskadaniel@gmail.com"))
                .build();
    }
}
