package com.enzode.stockphoto.photoservices.jsonplaceholder.model;

import lombok.Data;

@Data
public class JphPhoto {
    private Long id;
    private Long albumId;
    private String title;
    private String url;
    private String thumbnailUrl;
}
