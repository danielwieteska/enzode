package com.enzode.stockphoto.photoservices.jsonplaceholder.mapping;

import com.enzode.stockphoto.domain.model.Photo;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphPhoto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface JphPhotoMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "user", ignore = true),
            @Mapping(target = "albums", ignore = true)
    })
    Photo jphPhotoToPhoto(JphPhoto jphPhoto);
}
