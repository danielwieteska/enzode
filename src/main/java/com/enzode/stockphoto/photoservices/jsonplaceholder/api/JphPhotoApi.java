package com.enzode.stockphoto.photoservices.jsonplaceholder.api;

import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphPhoto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(
        value = "JPHPhotoApi",
        url = "${stock-photo.json-place-holder.main-api-url}"
)
public interface JphPhotoApi {
    @GetMapping("/albums/{albumId}/photos")
    List<JphPhoto> getAlbumPhotos(@PathVariable Long albumId);
}
