package com.enzode.stockphoto.photoservices.jsonplaceholder.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JphCompany {
    private String name;
    private String catchPhrase;
    private String bs;
}
