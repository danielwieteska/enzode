package com.enzode.stockphoto.photoservices.jsonplaceholder.api;

import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphAlbum;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(
        value = "JphAlbumApi",
        url = "${stock-photo.json-place-holder.main-api-url}"
)
public interface JphAlbumApi {
    @GetMapping("/users/{userId}/albums")
    List<JphAlbum> getAllUserAlbums(@PathVariable Long userId);
}
