package com.enzode.stockphoto.photoservices.jsonplaceholder.mapping;

import com.enzode.stockphoto.domain.model.Address;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphAddress;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface JphAddressMapper {

    @Mapping(target = "id", ignore = true)
    Address jphAddressToAddress(JphAddress jphAddress);
}
