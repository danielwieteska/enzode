package com.enzode.stockphoto.photoservices.jsonplaceholder.mapping;

import com.enzode.stockphoto.domain.model.Album;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphAlbum;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface JphAlbumMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "user", ignore = true)
    })
    Album jphAlbumToAlbum(JphAlbum jphAlbum);
}
