package com.enzode.stockphoto.photoservices.jsonplaceholder.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class JphAlbum {
    @EqualsAndHashCode.Exclude
    private Long id;
    private Long userId;
    private String title;
}
