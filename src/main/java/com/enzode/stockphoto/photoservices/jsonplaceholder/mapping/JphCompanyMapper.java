package com.enzode.stockphoto.photoservices.jsonplaceholder.mapping;

import com.enzode.stockphoto.domain.model.Company;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphCompany;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface JphCompanyMapper {

    @Mapping(target = "id", ignore = true)
    Company jphCompanyToCompany(JphCompany jphCompany);
}
