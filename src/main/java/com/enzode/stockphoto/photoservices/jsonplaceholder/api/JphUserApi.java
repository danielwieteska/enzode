package com.enzode.stockphoto.photoservices.jsonplaceholder.api;

import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(
        value = "JPHUserApi",
        url = "${stock-photo.json-place-holder.main-api-url}/users"
)
public interface JphUserApi {
    @GetMapping
    List<JphUser> getAllUsers();
}
