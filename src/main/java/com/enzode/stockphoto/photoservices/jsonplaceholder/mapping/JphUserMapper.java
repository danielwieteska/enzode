package com.enzode.stockphoto.photoservices.jsonplaceholder.mapping;

import com.enzode.stockphoto.domain.model.User;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface JphUserMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "role", ignore = true),
            @Mapping(target = "password", ignore = true),
            @Mapping(target = "address.id", ignore = true),
            @Mapping(target = "company.id", ignore = true)
    })
    User jphUserToUser(JphUser jphUser);
}
