package com.enzode.stockphoto.photoservices.jsonplaceholder.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JphUser {
    private Long id;
    private String name;
    private String username;
    private String email;
    private String phone;
    private String website;
    private JphAddress address;
    private JphCompany company;
}
