package com.enzode.stockphoto.photoservices.jsonplaceholder.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JphAddress {
    private String street;
    private String suite;
    private String city;
    @JsonProperty(value = "zipcode")
    private String zipCode;
    private JphGeo geo;
}
