package com.enzode.stockphoto.photoservices.jsonplaceholder.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JphGeo {
    private String lat;
    private String lng;
}
