package com.enzode.stockphoto.photoservices.jsonplaceholder.mapping;

import com.enzode.stockphoto.domain.model.Geo;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphGeo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Mapper(componentModel = "spring")
public interface JphGeoMapper {

    @Mappings({
            @Mapping(target = "lat", source = "jphGeo.lat", qualifiedByName = "stringToBigDecimal"),
            @Mapping(target = "lng", source = "jphGeo.lng", qualifiedByName = "stringToBigDecimal")
    })
    Geo jphGeoToGeo(JphGeo jphGeo);

    @Named("stringToBigDecimal")
    static BigDecimal stringToBigDecimal(String bigDecimalAsString) {
        return new BigDecimal(bigDecimalAsString).setScale(4, RoundingMode.HALF_UP);
    }
}
