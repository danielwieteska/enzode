package com.enzode.stockphoto.synchronization;

import com.enzode.stockphoto.domain.model.Album;
import com.enzode.stockphoto.domain.model.Photo;
import com.enzode.stockphoto.domain.model.User;
import com.enzode.stockphoto.photoservices.jsonplaceholder.api.JphPhotoApi;
import com.enzode.stockphoto.photoservices.jsonplaceholder.mapping.JphPhotoMapper;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphPhoto;
import com.enzode.stockphoto.domain.repositories.PhotoRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
class PhotoSynchronizer {
    private final AlbumSynchronizer albumSynchronizer;
    private final PhotoRepository photoRepository;
    private final JphPhotoApi jphPhotoApi;
    private final JphPhotoMapper jphPhotoMapper;

    void synchronizePhotos() {
        log.info("Start synchronize photos");
        Map<User, List<Album>> userToAlbums = albumSynchronizer.getJphAlbumIdToAlbum().values().stream()
                .collect(Collectors.groupingBy(Album::getUser));

        for (User user : userToAlbums.keySet()) {
            List<Album> userAlbums = userToAlbums.get(user);
            List<JphPhoto> allUserPhotos = collectAllUserPhotos(userAlbums);
            Map<Long, Set<Album>> jphPhotoIdToAlbums = allUserPhotos.stream()
                    .collect(Collectors.groupingBy(
                            JphPhoto::getId,
                            Collectors.mapping(
                                    jphPhoto -> albumSynchronizer.getJphAlbumIdToAlbum().get(jphPhoto.getAlbumId()),
                                    Collectors.toSet()
                            )
                    ));
            Map<Long, JphPhoto> jphPhotoIdToJphPhoto = allUserPhotos.stream()
                    .collect(Collectors.toMap(JphPhoto::getId, jphPhoto -> jphPhoto));

            for (Long jphPhotoId : jphPhotoIdToAlbums.keySet()) {
                JphPhoto jphPhoto = jphPhotoIdToJphPhoto.get(jphPhotoId);
                Photo photo = createPhoto(jphPhoto, user, jphPhotoIdToAlbums);
                log.info("Saving photo -> url: {}, belonging to {} albums", photo.getUrl(), photo.getAlbums().size());
                photoRepository.save(photo);
            }
        }
        log.info("End synchronize photos");
    }

    private List<JphPhoto> collectAllUserPhotos(List<Album> userAlbums) {
        List<JphPhoto> allUserPhotos = new ArrayList<>();
        for (Album album : userAlbums) {
            log.info("Loading photos from album: albumId={}", album.getId());
            Long jphAlbumId = albumSynchronizer.getAlbumToJphAlbumId().get(album);
            List<JphPhoto> allAlbumPhoto = jphPhotoApi.getAlbumPhotos(jphAlbumId);
            allUserPhotos.addAll(allAlbumPhoto);
        }
        return allUserPhotos;
    }

    private Photo createPhoto(JphPhoto jphPhoto, User user, Map<Long, Set<Album>> jphPhotoIdToAlbums) {
        Photo photo = jphPhotoMapper.jphPhotoToPhoto(jphPhoto);
        photo.setUser(user);
        photo.setAlbums(new HashSet<>());
        photoRepository.findByUrl(photo.getUrl()).ifPresent(photoFromDb -> {
            photo.setId(photoFromDb.getId());
            photo.setAlbums(photoFromDb.getAlbums());
        });
        Set<Album> presentPhotoAlbums = jphPhotoIdToAlbums.get(jphPhoto.getId());
        photo.getAlbums().addAll(presentPhotoAlbums);
        return photo;
    }
}
