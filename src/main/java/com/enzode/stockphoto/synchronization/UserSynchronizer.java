package com.enzode.stockphoto.synchronization;

import com.enzode.stockphoto.domain.model.User;
import com.enzode.stockphoto.photoservices.jsonplaceholder.api.JphUserApi;
import com.enzode.stockphoto.photoservices.jsonplaceholder.mapping.JphUserMapper;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphUser;
import com.enzode.stockphoto.domain.repositories.AddressRepository;
import com.enzode.stockphoto.domain.repositories.CompanyRepository;
import com.enzode.stockphoto.domain.repositories.UserRepository;
import com.enzode.stockphoto.services.PasswordGenerator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
class UserSynchronizer {
    private final PasswordGenerator passwordGenerator;
    private final JphUserApi jphUserApi;
    private final JphUserMapper jphUserMapper;
    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final AddressRepository addressRepository;
    @Getter
    private Map<Long, User> jphUserIdToUser;

    void synchronizeUsers() {
        log.info("Start synchronize users");
        Set<User> existingUsers = new HashSet<>(userRepository.findAll());
        Set<JphUser> jphUsers = new HashSet<>(jphUserApi.getAllUsers());
        Set<User> presentUsers = jphUsers.stream()
                .map(jphUserMapper::jphUserToUser)
                .collect(Collectors.toSet());
        Set<User> newUsers = selectNewUsers(existingUsers, presentUsers);
        mergeUsers(newUsers);
        newUsers.forEach(this::addUserAndSendPassword);
        log.info("Number of new users: {}", newUsers.size());
        buildMap(jphUsers);
    }

    void flushData() {
        jphUserIdToUser = null;
    }

    private Set<User> selectNewUsers(Set<User> existingUsers, Set<User> presentUsers) {
        Set<User> newUsers = new HashSet<>();
        for (User potentiallyNewUser : presentUsers) {
            if (!existingUsers.contains(potentiallyNewUser)) {
                newUsers.add(potentiallyNewUser);
            }
        }
        return newUsers;
    }

    private void addUserAndSendPassword(User user) {
        String password = passwordGenerator.generateNewPassword();
        user.setPassword(password);
        user.setRole("PHOTO_VIEWER");
        userRepository.save(user);
        log.info("Sending new password to user: {} -> {}", password, user.getEmail());
    }

    private void mergeUsers(Set<User> newUsers) {
        for (User newUser : newUsers) {
            companyRepository.findByName(newUser.getCompany().getName())
                    .ifPresent(newUser::setCompany);
            addressRepository.findAddress(newUser.getAddress())
                    .ifPresent(newUser::setAddress);
        }
    }

    private void buildMap(Set<JphUser> jphUsers) {
        jphUserIdToUser = new HashMap<>();
        for (JphUser jphUser : jphUsers) {
            User user = userRepository.findUser(jphUser);
            jphUserIdToUser.put(jphUser.getId(), user);
        }
    }
}
