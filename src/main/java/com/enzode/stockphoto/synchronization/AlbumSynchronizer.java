package com.enzode.stockphoto.synchronization;

import com.enzode.stockphoto.domain.model.Album;
import com.enzode.stockphoto.domain.model.User;
import com.enzode.stockphoto.photoservices.jsonplaceholder.api.JphAlbumApi;
import com.enzode.stockphoto.photoservices.jsonplaceholder.model.JphAlbum;
import com.enzode.stockphoto.domain.repositories.AlbumRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
class AlbumSynchronizer {
    private final UserSynchronizer userSynchronizer;
    private final JphAlbumApi jphAlbumApi;
    private final AlbumRepository albumRepository;
    @Getter
    private Map<Long, Album> jphAlbumIdToAlbum;
    @Getter
    private Map<Album, Long> albumToJphAlbumId;

    void synchronizeAlbums() {
        log.info("Start synchronize albums");
        jphAlbumIdToAlbum = new HashMap<>();
        albumToJphAlbumId = new HashMap<>();
        for (Long jphUserId : userSynchronizer.getJphUserIdToUser().keySet()) {
            User user = userSynchronizer.getJphUserIdToUser().get(jphUserId);
            List<JphAlbum> jphAlbums = jphAlbumApi.getAllUserAlbums(jphUserId);
            Set<String> presentUserAlbums = jphAlbums.stream()
                    .map(JphAlbum::getTitle)
                    .collect(Collectors.toSet());
            Set<String> existingUserAlbums = albumRepository.findByUserId(user.getId())
                    .stream()
                    .map(Album::getTitle)
                    .collect(Collectors.toSet());
            Set<String> newUserAlbums = presentUserAlbums.stream()
                    .filter(x -> !existingUserAlbums.contains(x))
                    .collect(Collectors.toSet());
            saveNewAlbums(user, newUserAlbums);
            buildMaps(jphAlbums, user);
        }
        log.info("End synchronize albums");
    }

    void flushData() {
        jphAlbumIdToAlbum = null;
        albumToJphAlbumId = null;
    }

    private void saveNewAlbums(User user, Set<String> newUserAlbums) {
        List<Album> newAlbums = newUserAlbums.stream()
                .map(x -> Album.builder()
                        .user(user)
                        .title(x)
                        .build()
                ).collect(Collectors.toList());
        if (newAlbums.size() > 0) {
            log.info("UserId: {} -> number of new albums: {}", user.getId(), newAlbums.size());
            albumRepository.saveAll(newAlbums);
        }
    }

    private void buildMaps(List<JphAlbum> jphAlbums, User user) {
        for (JphAlbum jphAlbum : jphAlbums) {
            Album album = albumRepository.findByUserIdAndTitle(user.getId(), jphAlbum.getTitle());
            jphAlbumIdToAlbum.put(jphAlbum.getId(), album);
            albumToJphAlbumId.put(album, jphAlbum.getId());
        }
    }
}
