package com.enzode.stockphoto.synchronization;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class Synchronizer {
    private final UserSynchronizer userSynchronizer;
    private final AlbumSynchronizer albumSynchronizer;
    private final PhotoSynchronizer photoSynchronizer;

    @Scheduled(
            fixedRateString = "${stock-photo.synchronization.interval}",
            initialDelayString = "${stock-photo.synchronization.initial-delay}"
    )
    public void synchronize() {
        log.info("Start synchronize with JsonPlaceholder");
        userSynchronizer.synchronizeUsers();
        albumSynchronizer.synchronizeAlbums();
        userSynchronizer.flushData();
        photoSynchronizer.synchronizePhotos();
        albumSynchronizer.flushData();
        log.info("End synchronize with JsonPlaceholder");
    }

}
