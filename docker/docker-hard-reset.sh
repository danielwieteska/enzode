#!/usr/bin/env bash
sudo rm -rf /tmp/stock-photo*
docker container rm $(docker container ls -qa) --force
#docker image rm $(docker image ls -aq) --force
docker network rm $(docker network ls -q)
docker volume rm $(docker volume ls -q) --force
#