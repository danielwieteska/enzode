#!/usr/bin/env bash
imageVersion="1.0"
gradle clean bootJar --settings-file=../../settings.gradle
docker image build --tag enzode.com/stock-photo:${imageVersion} ../..
#
# we can also publish docker image to Enzode private docker repository:
#docker push enzode.com/stock-photo:${imageVersion}
#
