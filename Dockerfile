FROM openjdk:11.0.7-jre-slim-buster
#
ENV APP_PTH="/stock-photo/app.jar" \
    LOGGING_FILE_PATH="/stock-photo/logs" \
    LOGGING_FILE_NAME="/stock-photo/logs/app.log"
#
COPY ./build/libs/*.jar ${APP_PTH}
#
ENTRYPOINT java -Xmx4G -jar ${APP_PTH}
#